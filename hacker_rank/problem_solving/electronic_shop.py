def get_money_spent(keyboards, drives, budget):
    result = -1
    for keyboard in keyboards:
        for drive in drives:
            if keyboard + drive <= budget:
                result = max(result, keyboard + drive)


if __name__ == '__main__':
    b = int(input().split()[0])

    keyboards = list(map(int, input().rstrip().split()))

    drives = list(map(int, input().rstrip().split()))

    print(get_money_spent(keyboards, drives, b))
