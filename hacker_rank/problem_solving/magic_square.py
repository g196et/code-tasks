def forming_magic_square(s):
    diffs = []
    all_possible = [
            [[8, 1, 6], [3, 5, 7], [4, 9, 2]],
            [[6, 1, 8], [7, 5, 3], [2, 9, 4]],
            [[4, 9, 2], [3, 5, 7], [8, 1, 6]],
            [[2, 9, 4], [7, 5, 3], [6, 1, 8]],
            [[8, 3, 4], [1, 5, 9], [6, 7, 2]],
            [[4, 3, 8], [9, 5, 1], [2, 7, 6]],
            [[6, 7, 2], [1, 5, 9], [8, 3, 4]],
            [[2, 7, 6], [9, 5, 1], [4, 3, 8]],]

    for possibility in all_possible:
        cost = 0
        for possible_row, s_row in list(zip(possibility, s)):
            for p_num, s_num in (list(zip(possible_row, s_row))):
                if p_num != s_num:
                    cost += abs(p_num - s_num)
        diffs.append(cost)
    return min(diffs)


if __name__ == '__main__':
    s = []

    for _ in range(3):
        s.append(list(map(int, input().rstrip().split())))

    result = forming_magic_square(s)

    print(result)
