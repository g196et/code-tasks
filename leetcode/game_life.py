"""https://leetcode.com/problems/game-of-life/"""
from typing import List


class Solution:
    def get_neighbors_count(self, board, m, n, changed):
        lines = []
        if m != 0:
            lines.append(m - 1)
        lines.append(m)
        if m != len(board) - 1:
            lines.append(m + 1)
        cols = []
        if n != 0:
            cols.append(n - 1)
        cols.append(n)
        if n != len(board[m]) - 1:
            cols.append(n + 1)
        result = 0
        for line in lines:
            for col in cols:
                if (line, col) == (m, n):
                    continue
                current_value = board[line][col]
                if (line, col) in changed:
                    current_value = 1 if current_value == 0 else 0
                if current_value == 1:
                    result += 1
                if result > 3:
                    return result
        return result
    
    def gameOfLife(self, board: List[List[int]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        changed = set()
        for m, m_v in enumerate(board):
            for n, n_v in enumerate(board[m]):
                neighbors_count = self.get_neighbors_count(board, m, n, changed)
                print(m, n, neighbors_count)
                if n_v == 0 and neighbors_count == 3:
                    board[m][n] = 1
                    changed.add((m, n))
                elif n_v == 1 and (neighbors_count < 2 or neighbors_count > 3):
                    board[m][n] = 0
                    changed.add((m, n))
                    

board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
Solution().gameOfLife(board)
print(board)
print(board == [[0,0,0],[1,0,1],[0,1,1],[0,1,0]])