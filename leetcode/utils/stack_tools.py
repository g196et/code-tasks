class MinStack:
    def __init__(self):
        self.val = []

    def push(self, x: int) -> None:
        current_min = self.getMin()
        current_min = x if current_min is None or x < current_min else current_min
        self.val.append([x, current_min])

    def pop(self) -> None:
        if self.val:
            self.val.pop()

    def top(self) -> int:
        if self.val:
            return self.val[len(self.val) - 1][0]

    def getMin(self) -> int:
        if self.val:
            return self.val[len(self.val) - 1][1]


if __name__ == '__main__':
    pass
