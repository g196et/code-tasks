"""Классы и методы для работы со списками и нодами"""


class ListNode:
    """Простой класс одной ноды для односвязного списка"""
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        return str(self.val)

    def print_list(self):
        current_node = self
        result = []
        while current_node:
            result.append(current_node.val)
            current_node = current_node.next
        print(' -> '.join(map(str, result)))

    @classmethod
    def create_single_node_by_list(cls, nodes_list):
        """Сгенерировать список по списку значений.

        Args:
            nodes_list  (list[Any]): Список чего хотите.

        Returns:
            LinkNode: Головной узел.
        """
        if not nodes_list:
            return

        #  Запомним голову списка.
        head_node = cls(nodes_list[0])
        current_node = head_node

        for val in nodes_list[1:]:
            current_node.next = ListNode(val)
            current_node = current_node.next

        return head_node

    @classmethod
    def generate_single_node_list(cls, count):
        """Метод создания односвязного списка. Возвращает голову.

        Args:
            count (int): Кол-во узлов в списке.

        Returns:
            ListNode: Головной узел.
        """
        if count <= 0:
            return

        # Запомним голову списка.
        head_node = cls(1)
        current_node = head_node

        for i in range(count - 1):
            current_node.next = ListNode(i + 2)
            current_node = current_node.next

        return head_node
