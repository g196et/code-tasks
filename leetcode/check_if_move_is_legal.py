"""
# 1958
https://leetcode.com/problems/check-if-move-is-legal/
"""
DIRECTION = [
    # (0, 1), # right
    # (1, 1), # right down diagonal
    # (1, 0), # down
    # (1, -1), # left down diagonal
    # (0, -1), # left
    (-1, -1), # left up diagonal
    # (-1, 0), # up
    # (-1, 1), # right up diagonal
]

class Solution(object):
    def checkMove(self, board, rMove, cMove, color):
        """
        :type board: List[List[str]]
        :type rMove: int
        :type cMove: int
        :type color: str
        :rtype: bool
        """
        def check_dicrection(r, c):
            count = 2
            new_r = rMove + r
            new_c = cMove + c
            while 0 <= new_r < len(board) and 0 <= new_c < len(board[0]):
                if board[new_r][new_c] == '.':
                    return False
                if count < 3 and board[new_r][new_c] == color:
                    return False
                if count >= 3 and board[new_r][new_c] == color:
                    return True
                new_r += r
                new_c += c
                count += 1
            return False
        
        for direction in DIRECTION:
            if check_dicrection(*direction):
                return True
        return False


args = [
    # [["W","W",".","W",".",".","B","."],[".","B","W","B","B",".",".","."],["B",".","B",".","W","B","W","B"],["B","B","W","W","B","B","W","W"],["B","B","B","B","W",".",".","."],["B","W","W","W",".",".","B","B"],[".",".","B",".","B","B","W","W"],["B",".","B","B","B","B",".","W"]],
    # 0,
    # 7,
    # "B",
    [
        ["B","W",".","B","W","W","B","."],
        ["B",".",".","B","W","W",".","."],
        ["W","W",".","B","B",".","B","W"],
        ["B","W","B",".","B",".","B","B"],
        ["B","W","W","B",".","W","B","B"],
        ["W","W",".","B","W","B",".","."],
        ["W",".","B","W","W","B",".","B"],
        ["W",".","B","B",".","B",".","."]
    ],
    2,
    5,
    "B"
]
result = Solution().checkMove(*args)
print(result)
