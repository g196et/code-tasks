"""https://leetcode.com/problems/unique-paths-ii/"""
from collections import deque

class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid):
        m = len(obstacleGrid)
        n = len(obstacleGrid[0])
        res = [[0 for _ in range(n)] for _ in range(m)]
        res[0][0] = 1
        s = deque([(0, 0)])
        v = []
        while s:
            x, y = s.popleft()
            if (x, y) in v:
                continue
            if x >= m or y >= n:
                continue
            if obstacleGrid[x][y] == 1:
                continue
            if x - 1 >= 0 and obstacleGrid[x-1][y] != 1:
                res[x][y] += res[x-1][y]
            if y - 1 >= 0 and obstacleGrid[x][y-1] != 1:
                res[x][y] += res[x][y-1]
            s.append((x+1, y))
            s.append((x, y+1))
            v.append((x,y))

        return res[m-1][n-1]


if __name__ == '__main__':
    obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]

    print(Solution().uniquePathsWithObstacles(obstacleGrid))