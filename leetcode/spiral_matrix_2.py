"""https://leetcode.com/problems/spiral-matrix-ii/"""

from typing import List

mapping = {
    'l': [0, -1],
    'r': [0, 1],
    'u': [-1, 0],
    'd': [1, 0],
}
weight = {
    'ul': 'l',
    'ur': 'u',
    'dr': 'r',
    'dl': 'd'
}

class Solution:
    def get_direction(self, m, x, y):
        sides = []
        if x != 0 and m[x-1][y] is None:
            sides.append('u')
        if x+1 != len(m) and m[x+1][y] is None:
            sides.append('d')
        if y+1 != len(m) and m[x][y+1] is None:
            sides.append('r')
        if y != 0 and m[x][y-1] is None:
            sides.append('l')
        if not sides:
            return None
        return mapping[
            weight.get(''.join(sides), sides[0])
        ]

    def generateMatrix(self, n: int) -> List[List[int]]:
        m = [[None for _ in range(n)] for _ in range(n)]
        value = 1
        x, y = 0, 0
        direction= self.get_direction(m, x, y)
        while (direction):
            m[x][y] = value
            value += 1
            x += direction[0]
            y += direction[1]
            direction= self.get_direction(m, x, y)

        m[x][y] = value

        return m


res = Solution().generateMatrix(3)
print(res)