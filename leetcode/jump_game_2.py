from typing import List

class SolutionTmp:
    """Решение через обход графа + рекурсия. Очень медленно."""
    def rec(self, nums, visited, current_index, completed):
        if current_index >= len(nums) - 1:
            return 0
        if current_index in completed:
            return visited.get(current_index)
        if nums[current_index] == 0:
            return None
        for value in range(1, nums[current_index] + 1):
            result = self.rec(nums, visited, current_index + value, completed)
            if result is None:
                continue
            result += 1
            if current_index not in visited or result < visited[current_index]:
                visited[current_index] = result
        completed.add(current_index)
        return visited.get(current_index)
        
    
    def jump(self, nums: List[int]) -> int:
        return self.rec(nums, {}, 0, set())


class Solution:
    """Жадный алгоритм""" 
    def jump(self, nums: List[int]) -> int:
        current = 0
        jump = 0
        result = 0
        for index in range(len(nums)-1):
            jump = max(index + nums[index], jump)
            if index == current:
                current = jump
                result += 1
        return result

# print(Solution().jump([5,9,3,2,1,0,2,3,3,1,0,0]))
print(Solution().jump([2,3,1,1,4]))
