class Node:
    def __init__(self, v):
        self.data = []
        self.value = v
        self.can_be_end = False

    def __repr__(self) -> str:
        return f'{self.value} -> {[item.value for item in self.data]}'

class WordDictionary:
    def __init__(self):
        self.root = Node('')

    def addWord(self, word: str) -> None:
        node = self.root
        for index, c in enumerate(word):
            add = False
            for item in node.data:
                if item.value == c:
                    node = item
                    add = True
                    break
            if not add:
                item = Node(c)
                node.data.append(item)
                node = item
            if index == len(word) - 1:
                node.self.can_be_end = True

    def search(self, word: str, index=0, node=None) -> bool:
        if node is None:
            node = self.root
        if index == len(word):
            return not node.data or node.can_be_end
        c = word[index]
        for item in node.data:
            if (c != '.' and item.value == c) or (c == '.'):
                result = self.search(word, index+1, item)
                if result:
                    return True


if __name__ == '__main__':
    t = WordDictionary()
    t.addWord('bad')
    t.addWord('dad')
    t.addWord('mad')

    print(t.search('a'))
    print(t.search('b..'))
    print(t.search('b.m'))
    # c = ["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
    # a = [[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]