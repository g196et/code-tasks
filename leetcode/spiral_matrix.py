DIRS = {'r': (0, 1), 'd': (1, 0), 'l': (0, -1), 'u': (-1, 0)}
NEXT_DIR = {'r': 'd', 'd': 'l', 'l': 'u', 'u': 'r'}

class Solution:
    def spiralOrder(self, matrix):
        l = 0
        cur_dir = 'r'
        x = 0
        y = 0
        res = []
        m = len(matrix)
        n = len(matrix[0])
        while len(res) != m * n:
            res.append(matrix[x][y])
            nx = x + DIRS[cur_dir][0]
            ny = y + DIRS[cur_dir][1]
            change = False
            if nx in [m-l, 0+l] and cur_dir in 'ud':
                change = True
            else:
                x = nx
            if ny in [n-l, -1+l] and cur_dir in 'lr':
                change = True
            else:
                y = ny
            if change:
                if cur_dir == 'u':
                    l += 1
                cur_dir = NEXT_DIR[cur_dir]
                x = x + DIRS[cur_dir][0]
                y = y + DIRS[cur_dir][1]

        return res


if __name__ == '__main__':
    mat = [[1,2,3],[4,5,6],[7,8,9]]
    print(Solution().spiralOrder(mat))