from leetcode.utils.node_list_tools import ListNode


class Solution:
    def reorderList(self, head) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        if not head.next or not head.next.next:
            return head
        s = []
        p = head
        while p:
            s.append(p)
            p = p.next
        cur = head
        while True:
            tail = s.pop()
            if tail == cur or tail == cur.next:
                tail.next = None
                break
            tail.next, cur.next  = cur.next, tail
            cur = tail.next
        return head

if __name__ == '__main__':
    print('start')
    head = ListNode.create_single_node_by_list([1,2,3,4,5])
    head = Solution().reorderList(head)
    head.print_list()
    head = ListNode.create_single_node_by_list([1,2,3,4,5,6])
    head = Solution().reorderList(head)
    head.print_list()
