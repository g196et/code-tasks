"""https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/description/"""
from utils.node_list_tools import ListNode
from utils.tree_tools import TreeNode


class Solution:
    def sortedListToBST(self, head):
        def t(l ,r):
            if l is None:
                return None
            s = l
            f = l
            while f is not r and f.next is not r:
                s = s.next
                f = f.next.next
            if s is f:
                return TreeNode(s.val)
            root = TreeNode(s.val)
            root.left = t(l, s)
            if s.next is not r:
                root.right = t(s.next, r)
            return root
        res = t(head, None)
        return res


if __name__ == '__main__':
    # inp = ListNode.create_single_node_by_list([-10,-3,0,5,9])
    # inp = ListNode.create_single_node_by_list([])
    # inp = ListNode.create_single_node_by_list([-1])
    inp = ListNode.create_single_node_by_list([x for x in range(100)])
    res = Solution().sortedListToBST(inp)
    print(res.display('file.txt'))
