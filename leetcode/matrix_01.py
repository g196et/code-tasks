'''https://leetcode.com/problems/01-matrix/description/'''
'''НЕРЕШЕНО!'''
from collections import deque


class Solution:
    def it(self, x, y, mat):
        m = len(mat)
        n = len(mat[0])
        for dx, dy in ((1, 0), (0, -1), (-1, 0), (0, 1)):
            if 0 <= x + dx < m and 0 <= y + dy < n:
                yield x + dx, y + dy

    def updateMatrix(self, mat, result=None, visited=None, node=None):
        if result is None:
            result = [[None] * len(mat[0]) for _ in range(len(mat))]
            visited = deque()
            visited.append((0, 0))
        while visited:
            node = visited.popleft()
            x = node[0]
            y = node[1]
            if (x, y) == (2, 7):
                print(1)
            v = mat[x][y]
            res = set()
            na = 1
            if v == 0:
                res.add(0)
                na = 1 << 4
            for dx, dy in self.it(x, y, mat):
                if (dx, dy) not in visited and result[dx][dy] is None:
                    visited.append((dx, dy))
                else:
                    na =  na << 1
                if v == 0:
                    continue
                if mat[dx][dy] == 0:
                    res.add(1)
                elif result[dx][dy] is not None:
                    dv = result[dx][dy] + 1
                    res.add(dv)
            if na == 1 << 4:
                result[x][y] = min(res) if res else None
            else:
                visited.append((x, y))

        return result


if __name__ == '__main__':
    # mat = [[0,0,0],[0,1,0],[0,0,0]]
    # mat = [[0,0,0],[0,1,0],[1,1,1]]
    # mat = [[0,0,0],[0,1,0],[1,1,1],[1,1,1],[1,1,1]]
    # mat = [[0],[0],[0],[0],[0]]
    mat = [[1,1,0,0,1,0,0,1,1,0],[1,0,0,1,0,1,1,1,1,1],[1,1,1,0,0,1,1,1,1,0],[0,1,1,1,0,1,1,1,1,1],[0,0,1,1,1,1,1,1,1,0],[1,1,1,1,1,1,0,1,1,1],[0,1,1,1,1,1,1,0,0,1],[1,1,1,1,1,0,0,1,1,1],[0,1,0,1,1,0,1,1,1,1],[1,1,1,0,1,0,1,1,1,1]]
    print(Solution().updateMatrix(mat))