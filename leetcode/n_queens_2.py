class Solution:
    def __init__(self):
        self._field = None

    def set_queen(self, row, col, val):
        n = len(self._field)
        self._field[row][col] += val
        for row_i in range(n):
            if row_i != row:
                self._field[row_i][col] += val
        for col_i in range(n):
            if col_i != col:
                self._field[row][col_i] += val
        row_i, col_i = row-1, col-1
        while row_i >= 0 and col_i >= 0:
            self._field[row_i][col_i] += val
            row_i -= 1
            col_i -= 1
        row_i, col_i = row+1, col+1
        while row_i < n and col_i < n:
            self._field[row_i][col_i] += val
            row_i += 1
            col_i += 1
        row_i, col_i = row-1, col+1
        while row_i >= 0 and col_i < n:
            self._field[row_i][col_i] += val
            row_i -= 1
            col_i += 1
        row_i, col_i = row+1, col-1
        while row_i < n and col_i >= 0:
            self._field[row_i][col_i] += val
            row_i += 1
            col_i -= 1

    def _attack(self, row, col):
        return self._field[row][col] > 0

    def totalNQueens(self, n: int) -> int:
        self._field = [[0] * n for _ in range(n)]
        return self._t(0, 0)

    def _t(self, row, count):
        n = len(self._field)
        for col in range(n):
            if not self._attack(row, col):
                self.set_queen(row, col, 1)
                if row == n - 1:
                    count += 1
                else:
                    count = self._t(row+1, count)
                self.set_queen(row, col, -1)
        return count

    def _p(self):
        for x in self._field:
            print(x)


if __name__ == '__main__':
    print(Solution().totalNQueens(4))