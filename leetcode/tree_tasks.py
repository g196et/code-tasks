from utils.tree_tools import TreeNode


class Solution:
    def search_bst_recursive(self, root, val: int):
        """Рекурсивный поиск в BST"""
        if root is None:
            return None
        if root.val == val:
            return root
        if root.val > val:
            return self.search_bst_recursive(root.left, val)
        else:
            return self.search_bst_recursive(root.right, val)
