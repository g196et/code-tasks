"""https://leetcode.com/problems/single-element-in-a-sorted-array/"""


class Solution:
    def singleNonDuplicate(self, nums):
        l = 0
        r = len(nums)
        while l <= r:
            mid = (l + r) // 2
            val = nums[mid]
            ln = nums[mid-1] if mid != 0 else None
            rn = nums[mid+1] if mid != len(nums) - 1 else None
            if ln != val and rn != val:
                return val
            if mid % 2 == 0:
                if rn != val:
                    r = mid - 1
                else:
                    l = mid + 1
            else:
                if ln == val:
                    l = mid + 1
                else:
                    r = mid - 1


if __name__ == '__main__':
    # res = Solution().singleNonDuplicate([1,1,2,3,3,4,4,8,8])
    # res = Solution().singleNonDuplicate([3,3,7,7,10,11,11])
    # res = Solution().singleNonDuplicate([0,3,3,7,7,10,10,11,11])
    res = Solution().singleNonDuplicate([3,3,7,7,10,10,11,11,12])
    print(res)