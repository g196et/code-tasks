from leetcode.utils.node_list_tools import ListNode


class Solution:
    def reverse_single_list_recursive(self, head: ListNode) -> ListNode or None:
        """Развернуть односвязный список рекурсивным алгоритмом."""
        if head is None or head.next is None:
            return head

        new_node = self.reverse_single_list_recursive(head.next)

        head.next.next = head
        head.next = None

        return new_node

    @staticmethod
    def reverse_single_list_iterative(head: ListNode) -> ListNode or None:
        """Развернуть односвязный список иттеративно"""
        prev = None
        current = head
        while current is not None:
            next_item = current.next
            current.next = prev
            prev = current
            current = next_item

        return prev

    @staticmethod
    def merge_two_lists(l1: ListNode, l2: ListNode) -> ListNode or None:
        """Смержить 2 упорядоченных списка"""
        if not l1:
            return l2
        elif not l2:
            return l1
        elif not l2 and not l1:
            return
        if l1.val < l2.val:
            start_link = ListNode(l1.val)
        else:
            start_link = ListNode(l2.val)
        current_link = start_link
        while l1 or l2:
            if l1 and (not l2 or l1.val <= l2.val):
                next_l1 = l1.next
                l1.next = None
                current_link.next = l1
                l1 = next_l1
            elif l2 and (not l1 or l2.val < l1.val):
                next_l2 = l2.next
                l2.next = None
                current_link.next = l2
                l2 = next_l2

            current_link = current_link.next

        return start_link.next

    @staticmethod
    def swap_node_in_pairs(head):
        """1,2,3,4,5,6 -> 2,1,4,3,6,5"""
        if not head or not head.next:
            return head
        result = head.next
        node = head
        prev_node = None
        while node:
            if not node.next:
                break
            cur_node = node
            next_node = node.next
            node.next = next_node.next
            next_node.next = cur_node
            if not prev_node:
                prev_node = cur_node
            else:
                prev_node.next = next_node
                prev_node = cur_node
            node = node.next

        return result
